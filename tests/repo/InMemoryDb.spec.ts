import {expect} from 'chai'
import {InMemoryDb} from "../../src/repo/InMemoryDb";
import {Book} from "../../src/models/Book";

describe('In-memory DB tests', () => {
    it('Should add successfully', () => {
        const book = new Book(1, 'Hello', 'Hi');
        const addCallback = () => InMemoryDb.add(book);
        expect(addCallback).to.not.throw();
    });

    it('Should search for existing items without throwing', () => {
        const book1 = new Book(1, 'Hello', 'Hi');
        InMemoryDb.add(book1);
        const book2 = new Book(315, 'Cool', 'nice');
        InMemoryDb.add(book2);

        const search1 = InMemoryDb.search(1);
        const search2 = InMemoryDb.search(315);

        expect(search1).to.deep.equal(book1);
        expect(search2).to.deep.equal(book2);
    });

    it('Should throw for non-existing book', () => {
        const book1 = new Book(1, 'Hello', 'Hi');
        InMemoryDb.add(book1);
        const book2 = new Book(315, 'Cool', 'nice');
        InMemoryDb.add(book2);

        const errorSearchCallback = () => InMemoryDb.search(31712);

        expect(errorSearchCallback).to.throw();
    })
})
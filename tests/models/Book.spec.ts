import {expect} from 'chai'
import {Book} from "../../src/models/Book";

describe('Book model tests', () => {
    it('Should be able to get properties', () => {
        // Arrange
        const book = new Book(1, 'Hello', 'Hi');

        // Assert
        expect(book.id).to.equal(1);
        expect(book.name).to.equal('Hello');
        expect(book.description).to.equal('Hi');
    });

    it('Setter should apply name when the length is less than 16', () => {
        // Arrange
        const book = new Book(1, 'Hello', 'Hi');
        const newName = 'Yeeter';

        // Act
        book.name = newName;

        // Assert
        expect(book.name).to.equal(newName);
        expect(book.id).to.equal(1);
        expect(book.description).to.equal('Hi');
    })

    it('Setter should set empty string when name length is larger than 16', () => {
        // Arrange
        const book = new Book(1, 'Hello', 'Hi');

        // Act
        book.name = 'Yeeeter of Worlds';

        // Assert
        expect(book.name).to.equal('');
        expect(book.id).to.equal(1);
        expect(book.description).to.equal('Hi');
    })
})
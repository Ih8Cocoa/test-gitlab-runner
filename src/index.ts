import {Book} from "./models/Book";
import {InMemoryDb} from "./repo/InMemoryDb";

function ready() {
    const book = new Book(1, 'Life of PI', 'Very good');
    const book0 = new Book(12, 'gg', 'ggg');

    InMemoryDb.add(book);
    InMemoryDb.add(book0);

    const searched = InMemoryDb.search(1);
    console.log(searched);

    const searched0 = InMemoryDb.search(12);
    console.log(searched0);
}

ready();
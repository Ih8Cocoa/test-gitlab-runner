export class Book {
    constructor(readonly id: number, private _name: string, private _description: string) {
    }

    get name() {
        return this._name;
    }

    get description() {
        return this._description;
    }

    set name(newName: string) {
        if (newName.length > 16) {
            this._name = '';
        } else {
            this._name = newName;
        }
    }
}
import {Book} from "../models/Book";

export class InMemoryDb {
    private static readonly _data: Map<number, Book> = new Map();

    public static add(book: Book): void {
        const {id} = book;
        InMemoryDb._data.set(id, book);
    }

    public static search(bookId: number): Book {
        const book = InMemoryDb._data.get(bookId);
        if (!book) {
            throw new Error('Book does not exist');
        }
        return book;
    }
}